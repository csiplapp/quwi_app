package com.quwi

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.quwi.adapter.CustomAdapter
import com.quwi.connection.ApiCallBack
import com.quwi.connection.ApiConnection
import com.quwi.databinding.ActivityProjectDetailsBinding
import com.quwi.helper.Constant
import com.quwi.helper.Utils
import com.quwi.viewholder.WorkerViewHolder
import com.squareup.picasso.Picasso
import okhttp3.Headers
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ProjectDetails : AppCompatActivity(), View.OnClickListener {

    private var customAdapter: CustomAdapter? = null
    var binding: ActivityProjectDetailsBinding? = null
    var jsonWorkerList = JSONArray()
    var projObj: JSONObject? = null
    var upDate: String? = null
    var userDelete = ArrayList<Int>()
    var userAdded = ArrayList<Int>()
    var projId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_project_details)
        initView()
        initToolBar()
    }

    private fun initToolBar() {
        binding!!.headerLayout.txtNavTitle.text = getString(R.string.lbl_details)
        binding!!.headerLayout.btnNav.setBackgroundResource(R.drawable.ic_baseline_arrow_24)
        binding!!.headerLayout.btnNav.setOnClickListener { finish() }
    }

    private fun initView() {

        projObj = JSONObject(intent.getStringExtra("projObj"))
        projId = projObj!!.getString("id")
        binding!!.userRecycler.setHasFixedSize(true)
        binding!!.userRecycler.layoutManager = LinearLayoutManager(this)
        binding!!.userRecycler.itemAnimator = DefaultItemAnimator()

        binding!!.txtDate.setOnClickListener(this)
        binding!!.btnUpdate.setOnClickListener(this)
        binding!!.imgDate.setOnClickListener(this)

        initAdapter()
        getProjDetailsCallApi()
    }


    private fun getProjDetailsCallApi() {
        ApiConnection.connectPost(
                this@ProjectDetails,
                Constant.TAG,
                true,
                null,
                ApiCallBack.projDetails(this@ProjectDetails, projId),
                object : ApiConnection.ConnectListener {
                    override fun onResponseSuccess(
                            response: String,
                            headers: Headers,
                            StatusCode: Int
                    ) {
                        try {
                            val jobject = JSONObject(response)
                            if (jobject.has("project") || !jobject.isNull("project")) {

                                val jsonProjResponse = jobject.getJSONObject("project")
                                jsonWorkerList = jsonProjResponse.getJSONArray("users")
                                customAdapter!!.notifyDataSetChanged()
                                setProjectDetails(jsonProjResponse)

                            } else {
                                Utils.showSnackBar(binding!!.linearParent, jobject.getString("message"))
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }

                    override fun onResponseFailure(
                            responseData: String,
                            headers: Headers,
                            StatusCode: Int
                    ) {
                        Utils.showSnackBar(binding!!.linearParent, responseData)
                    }

                    override fun onFailure(headers: Headers) {
                        Utils.showSnackBar(binding!!.linearParent, getString(R.string.failure))
                    }

                    override fun onConnectionFailure() {
                        Utils.showSnackBar(binding!!.linearParent, getString(R.string.errorCheckNet))
                    }

                    override fun onException(headers: Headers, StatusCode: Int) {
                        Utils.showSnackBar(binding!!.linearParent, getString(R.string.errorSomething))
                    }
                })
    }

    private fun setProjectDetails(jObj: JSONObject) {

        binding!!.switchProjStatus.isChecked = jObj.getInt("is_active") == 1
        binding!!.switchWatcher.isChecked = jObj.getInt("is_owner_watcher") == 1

//



        binding!!.edProjName.setText(jObj.getString("name"))
        binding!!.edProjName.setSelection(binding!!.edProjName.text!!.length)

        if (!jObj.isNull("dta_user_since")) {
            binding!!.txtDate.text = Utils.convertDateString(jObj.getString("dta_user_since"), false)
            upDate = jObj.getString("dta_user_since")

        } else {
            binding!!.txtDate.text = getString(R.string.lbl_all_time)
            upDate = ""
        }




        if (!jObj.isNull("logo_url")) {

            binding!!.txtProjTitle.visibility = View.GONE
            binding!!.imgProfile.visibility = View.VISIBLE
            Picasso.get().load(jObj.getString("logo_url"))
                    .placeholder(R.drawable.place_holder).error(
                            R.drawable.place_holder
                    ).into(binding!!.imgProfile)

        } else {

            binding!!.imgProfile.visibility = View.GONE
            binding!!.txtProjTitle.visibility = View.VISIBLE


//                    if (jObj.getString("name").contains(" ")) {
//                        holder.txtProjTitle.text = jObj.getString("name").substring(0, 1) + firstWord(jObj.getString("name"))!!.substring(1, 1)
//                    } else {
            binding!!.txtProjTitle.text = jObj.getString("name").substring(0, 1)
//                    }

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                binding!!.txtProjTitle.background.colorFilter = Utils.colorFilter()
            }
        }

    }

    private fun initAdapter() {
        customAdapter = CustomAdapter(object : CustomAdapter.AdapterListener {
            override fun onCreateViewHolder(
                    parent: ViewGroup,
                    viewType: Int
            ): RecyclerView.ViewHolder {
                return WorkerViewHolder(
                        LayoutInflater.from(parent.context)
                                .inflate(R.layout.user_item, parent, false)
                )
            }

            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                val jObj = jsonWorkerList.get(position) as JSONObject
                holder as WorkerViewHolder

                holder.txtWorkerName.text = jObj.getString("name")
                holder.txtWorkTitle.text = jObj.getString("name").substring(0, 1)

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                    holder.txtWorkTitle.background.colorFilter = Utils.colorFilter()
                }

                holder.chkUser!!.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                    val empId = jObj.getInt("id")
                    if (isChecked) {
                        userDelete.remove(empId)
                        userAdded.add(empId)
                    } else {
                        userDelete.add(empId)
                        userAdded.remove(empId)
                    }
                })
            }

            override val itemCount: Int
                get() = jsonWorkerList.length()

            override fun getItemViewType(position: Int): Int {
                return 0
            }

            override fun getItemId(position: Int): Long {
                return 0
            }

            override fun filterData(nameString: String) {

            }
        })
        binding!!.userRecycler.adapter = customAdapter
    }

    override fun onClick(v: View?) {
        when (v!!) {
            binding!!.txtDate -> {
                datePickerShow()
            }
            binding!!.btnUpdate -> {
                updateProjectCallApi()
            }
            binding!!.imgDate -> {
                binding!!.txtDate.text = getString(R.string.lbl_all_time)
            }
        }
    }

    private fun updateProjectCallApi() {
        val map = HashMap<String, String>()
        map["name"] = binding!!.edProjName.text.toString()
        map["is_active"] = if (binding!!.switchProjStatus.isChecked) "1" else "0"
        map["position"] = projObj!!.getString("position")
        map["dta_user_since"] = upDate!!
        map["is_owner_watcher"] = if (binding!!.switchWatcher.isChecked) "1" else "0"

//        map["id_users_add[]"] = userAdded
//        map["id_users_del[]"] = userDelete

        ApiConnection.connectPost(
                this@ProjectDetails,
                Constant.TAG,
                true,
                null,
                ApiCallBack.updateProjDetails(this@ProjectDetails, projId, map,userAdded,userDelete),
                object : ApiConnection.ConnectListener {
                    override fun onResponseSuccess(
                            response: String,
                            headers: Headers,
                            StatusCode: Int
                    ) {
                        try {
                            val jobject = JSONObject(response)
                            if (jobject.has("project") || !jobject.isNull("project")) {
                                setResult(Constant.RESULT_PROJECT_DETAILS)
                                finish()
                            } else {
                                Utils.showSnackBar(binding!!.linearParent, jobject.getString("message"))
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }

                    override fun onResponseFailure(
                            responseData: String,
                            headers: Headers,
                            StatusCode: Int
                    ) {
                        Utils.showSnackBar(binding!!.linearParent, responseData)
                    }

                    override fun onFailure(headers: Headers) {
                        Utils.showSnackBar(binding!!.linearParent, getString(R.string.failure))
                    }

                    override fun onConnectionFailure() {
                        Utils.showSnackBar(binding!!.linearParent, getString(R.string.errorCheckNet))
                    }

                    override fun onException(headers: Headers, StatusCode: Int) {
                        Utils.showSnackBar(binding!!.linearParent, getString(R.string.errorSomething))
                    }
                })
    }

    @SuppressLint("SimpleDateFormat")
    private fun datePickerShow() {
        val newCalendar = Calendar.getInstance()
        DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            val newDate: Calendar = Calendar.getInstance()
            val displayFormat = SimpleDateFormat(Constant.DATE_FORMAT.DISPLAY)
            newDate.set(year, monthOfYear, dayOfMonth)
            binding!!.txtDate.text = displayFormat.format(newDate.time)
            upDate = SimpleDateFormat(Constant.DATE_FORMAT.API).format(newDate.time)
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show()
    }

}