@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.quwi.helper

import android.content.Context
import org.json.JSONObject

object Constant {

    interface BASE_URL {
        companion object {
            val LIVE = "https://api.quwi.com/v2/"
        }
    }

    interface DATE_FORMAT {
        companion object {
            val API = "yyyy-MM-dd HH:mm:ss"
            val DISPLAY = "dd MMM yyyy"
            val DISPLAY_TIME = "dd MMM yyyy hh:mm a"
        }
    }

    interface API_NAME {
        companion object {
            const val LOGIN = "auth/login"
            const val GET_PROJECTS = "projects-manage"
            const val UPDATE_PROJECT = "projects-manage/update"
        }
    }


    fun setUserData(context: Context, userdetails: String) {
//        val jsonString = Gson().toJson(userdetails)
        val sharedPreferences = context.getSharedPreferences(PREFERENCES.USER, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(PREFERENCES.USER, userdetails)
        editor.apply()
    }


    val RESULT_PROJECT_DETAILS = 1001

    val TAG = "ProjManange"

    interface PREFERENCES {
        companion object {
            val USER = "userPrefs"
        }
    }

    fun clearUserData(context: Context) {
        val sharedPreferences = context.getSharedPreferences(Constant.PREFERENCES.USER, Context.MODE_PRIVATE)
        if (sharedPreferences.contains(Constant.PREFERENCES.USER)) {
            val editor = sharedPreferences.edit()
            editor.clear()
            editor.apply()
        }
    }

    fun checkUserData(context: Context): Boolean {
        val sharedPreferences = context.getSharedPreferences(Constant.PREFERENCES.USER, Context.MODE_PRIVATE)
        if (sharedPreferences.contains(Constant.PREFERENCES.USER)) {
            return true
        }
        return false
    }

    fun getUserData(context: Context): JSONObject? {
//        val gson = Gson()
        val sharedPreferences = context.getSharedPreferences(Constant.PREFERENCES.USER, Context.MODE_PRIVATE)
        if (sharedPreferences.contains(Constant.PREFERENCES.USER)) {
            val  userData = JSONObject(sharedPreferences.getString(Constant.PREFERENCES.USER, null))
//            val userData =
//                gson.fromJson<UserData>(
//                    sharedPreferences.getString(Constant.PREFERENCES.USER, null),
//                    UserData::class.java
//                )
            if (userData != null) {
                return userData
            }
        }
        return null
    }

}