package com.quwi.helper

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.Paint
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.transition.ChangeBounds
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.view.animation.DecelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import com.google.android.material.snackbar.Snackbar
import com.quwi.BuildConfig
import com.quwi.R
import com.wang.avi.AVLoadingIndicatorView
import java.net.Inet4Address
import java.net.NetworkInterface
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

object Utils {


    fun showSnackBar(coordinatorLayout: View, msg: String) {
//        val snack = Snackbar.make(findViewById(android.R.id.content), "Had a snack at Snackbar", Snackbar.LENGTH_LONG)
//        val view = snack.getView()
//        val params = view.getLayoutParams() as FrameLayout.LayoutParams
//        params.gravity = Gravity.TOP
//        view.setLayoutParams(params)
//        snack.show()
        Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_LONG).show()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    fun colorFilter (): BlendModeColorFilter {

        val rnd = Random()
        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

        return BlendModeColorFilter(
            color,
            BlendMode.SRC_ATOP
        )
    }

    fun convertDateString(dateString: String,isTime:Boolean): String {

        val outPutFormat =
                if(isTime) SimpleDateFormat(Constant.DATE_FORMAT.DISPLAY_TIME) else SimpleDateFormat(Constant.DATE_FORMAT.DISPLAY)

        try {
            val date = SimpleDateFormat(Constant.DATE_FORMAT.API).parse(dateString)
            return outPutFormat.format(date!!)
        } catch (e: ParseException) {
            e.printStackTrace()
            return ""
        }
    }


    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun checkLog(TAG: String, data: Any, throwable: Throwable?) {
        if (BuildConfig.DEBUG) {
            if (throwable != null) {
                Log.d(TAG + ">>", data.toString(), throwable)
            } else {
                Log.d(TAG + ">>", data.toString())
            }
        }
    }

    fun closeKeyboard(context: Context) {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            (context as Activity).window.decorView.rootView.windowToken,
            InputMethodManager.RESULT_UNCHANGED_SHOWN
        )
    }

    fun closeKeyboard(context: Context, dialog: Dialog) {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            dialog.window!!.decorView.rootView.windowToken,
            InputMethodManager.RESULT_UNCHANGED_SHOWN
        )
    }

    fun closeKeyBoardFragment(context: Context) {
        (context as Activity).getWindow().setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )
    }

    fun getLoader(context: Context): Dialog {

        val loaderDialog = Dialog(context)

        val window = loaderDialog.window
        window!!.requestFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawableResource(R.drawable.dialog_basic_transparent)
        loaderDialog.setContentView(R.layout.dialog_loader)
        window.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        loaderDialog.setCancelable(false)

        val loadingIndicatorView = loaderDialog.findViewById<AVLoadingIndicatorView>(R.id.spinLoader)

        loaderDialog.setOnShowListener { loadingIndicatorView.show() }

        loaderDialog.setOnDismissListener { loadingIndicatorView.hide() }

        return loaderDialog
    }
}


