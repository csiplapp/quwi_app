package com.quwi.connection


import android.content.Context
import okhttp3.ResponseBody
import retrofit2.Call
import java.util.ArrayList

object ApiCallBack {

    internal fun getRestInterface(context: Context): Api {
        val retrofit = ApiConnection.getClient(context)
        val restInterface = retrofit!!.create(Api::class.java)
        return restInterface
    }

    fun login(map: HashMap<String, String>, context: Context): Call<ResponseBody> {
        val callBack = getRestInterface(context).login(map)
        return callBack
    }

    fun getProjects(context: Context): Call<ResponseBody> {
        val callBack = getRestInterface(context).getProjects()
        return callBack
    }

    fun projDetails(context: Context, projId: String): Call<ResponseBody> {
        val callBack = getRestInterface(context).projDetails(projId)
        return callBack
    }

    fun updateProjDetails(context: Context, projId: String, map: HashMap<String, String>, userAdded: ArrayList<Int>, userDeleted: ArrayList<Int>): Call<ResponseBody> {
        val callBack = getRestInterface(context).updateProjDetails(projId, map,userAdded,userDeleted)
        return callBack
    }
}