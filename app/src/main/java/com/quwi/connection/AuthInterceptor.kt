package com.quwi.connection

import android.content.Context
import com.quwi.helper.Constant
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(context: Context) : Interceptor {

    val userData = Constant.getUserData(context)
    val barearToken = userData!!.getString("token")

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        // If token has been saved, add it to the request
        requestBuilder.addHeader("Authorization", "Bearer $barearToken")
        return chain.proceed(requestBuilder.build())
    }
}