package com.quwi.connection

import android.content.Context
import android.widget.Toast
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.quwi.helper.Constant
import com.quwi.helper.MapDeserializerDoubleAsIntFix
import com.quwi.helper.Utils
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.*
import java.lang.Exception

object ApiConnection {
    internal var retrofit: Retrofit? = null
    interface ConnectListener {

        fun onResponseSuccess(response: String, headers: Headers, StatusCode: Int)

        fun onResponseFailure(responseData: String, headers: Headers, StatusCode: Int)

        fun onFailure(headers: Headers)

        fun onConnectionFailure()

        fun onException(headers: Headers, StatusCode: Int)
    }

    fun connectPost(
            context: Context, TAG: String,
            isLoader: Boolean, refreshLayout: SwipeRefreshLayout?,
            connect: Call<ResponseBody>, listener: ConnectListener
    ) {

        if (!Utils.isNetworkAvailable(context)) {
            listener.onConnectionFailure()
            return
        }

        val loader = Utils.getLoader(context)
        if (isLoader) {
            loader.show()
        }

        if (refreshLayout != null) {
            if (!refreshLayout.isRefreshing) {
                refreshLayout.isRefreshing = true
            }
        }

        Utils.checkLog(TAG, "Params: " + connect.request().url.toString(), null)

        connect.enqueue(object : retrofit2.Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (isLoader) {
                    loader.dismiss()
                }

                if (refreshLayout != null) {
                    if (refreshLayout.isRefreshing) {
                        refreshLayout.isRefreshing = false
                    }
                }

                try {

                    val gsonBuilder = GsonBuilder()
                    gsonBuilder.registerTypeAdapter(object : TypeToken<Map<String, Any>>() {

                    }.getType(), MapDeserializerDoubleAsIntFix())

                    val jsonString = /*new Gson()*/gsonBuilder.create().toJson(response.body())
                    Utils.checkLog(TAG, "" + jsonString, null)


                    if (response.body() != null) {
                        listener.onResponseSuccess(response.body()!!.string(), response.headers(), response.code())
                    } else if(response.errorBody() !== null) {
                        listener.onResponseFailure(response.errorBody()!!.string(), response.headers(), response.code())
                    }

//                    val responseData = response.body()

//                    if (responseData!!.getStatus()) {
//                        listener.onResponseSuccess(/*new Gson()*/gsonBuilder.create().toJson(responseData.getData()),
//                            response.headers(),
//                            response.code()
//                        )
//                    }
                    /* else if (responseData.isSuccess() === false*//* Constants.STATUS_CODE.UNAUTHORIZED_ACCESS*//*) {
                      *//*  Utils.popToast(context, context.getString(R.string.sessionExpired))
                        Constant.clearUserData(context)
                        Utils.restartApp(context)*//*
                    }*/
//                    else {
//                        listener.onResponseFailure(responseData, response.headers(), response.code())
//                    }

                } catch (e: Exception) {
                    if (e != null) {
                        e.printStackTrace()
                        Utils.checkLog(TAG, "Exception: " + e.message, e.cause)
                        Toast.makeText(context, e.message.toString(), Toast.LENGTH_LONG).show()
                    }
                    listener.onException(response.headers(), response.code())

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                if (isLoader) {
                    loader.dismiss()
                }

                if (refreshLayout != null) {
                    if (refreshLayout.isRefreshing) {
                        refreshLayout.isRefreshing = false
                    }
                }

                if (call.isCanceled) {
                    Utils.checkLog(
                            "" +
                                    ">>", "CAncelled: " + call.toString(), t
                    )

                }


                Utils.checkLog(TAG, "Failure: " + call.toString(), t)
                listener.onFailure(call.request().headers)

            }
        })
    }


//
//    public static RequestBody createPartFromString(String descriptionString) {
//        if (descriptionString == null) {
//            descriptionString = "";
//        }
//        return RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
//    }


    fun getClient(context: Context): Retrofit? {
        // if (retrofit == null) {

        val gson = GsonBuilder()
                .setLenient()
                .create()

        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        var okHttpClient: OkHttpClient? = null

        if (Constant.checkUserData(context)) {
            okHttpClient = OkHttpClient().newBuilder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .addInterceptor(AuthInterceptor(context))
                    .build()
        } else {

            okHttpClient = OkHttpClient().newBuilder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .build()

        }


        retrofit = Retrofit.Builder()
                .baseUrl(Constant.BASE_URL.LIVE)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        return retrofit
    }


//
//    fun getClient1(url:String): Retrofit? {
//        // if (retrofit == null) {
//
//        val gson = GsonBuilder()
//                .setLenient()
//                .create()
//
//        val okHttpClient = OkHttpClient().newBuilder()
//                .connectTimeout(120, TimeUnit.SECONDS)
//                .readTimeout(120, TimeUnit.SECONDS)
//                .writeTimeout(120, TimeUnit.SECONDS)
//                .build()
//
//
//        retrofit1 = Retrofit.Builder()
//                .baseUrl(url)
//                .client(okHttpClient)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build()
//
//
//
//        Log.d( "Url: ",""+url)
//        // }
//        return retrofit1
//    }


//    fun prepareFilePart(partName: String, outputFile: ArrayList<String>): MultipartBody.Part {
//
//        var file = null
//        var requestFile = null
//
//        for(i in 0..outputFile.size) {
//
//
//
//             file = File(Uri.parse(outputFile.get(i)).getPath())
//             requestFile = RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), file)
//
//        }
//        return MultipartBody.Part.createFormData(partName, file.name, requestFile)
//    }


}