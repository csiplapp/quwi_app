package com.quwi.connection

import com.quwi.helper.Constant
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import java.util.ArrayList
import kotlin.collections.HashMap


interface Api {

    @FormUrlEncoded
    @POST(Constant.API_NAME.LOGIN)
    fun login(@FieldMap fieldMap: HashMap<String, String>): Call<ResponseBody>


    @GET(Constant.API_NAME.GET_PROJECTS)
    fun getProjects(): Call<ResponseBody>

    @GET(Constant.API_NAME.GET_PROJECTS + "/{id}")
    fun projDetails(@Path("id") projId: String): Call<ResponseBody>


    @FormUrlEncoded
    @POST(Constant.API_NAME.UPDATE_PROJECT)
    fun updateProjDetails(@Query("id") projId: String,
                          @FieldMap fieldMap: HashMap<String, String>,
                          @Field("id_users_add[]") userAdded: ArrayList<Int>,
                          @Field("id_users_del[]") userDelete: ArrayList<Int>

    ): Call<ResponseBody>

}