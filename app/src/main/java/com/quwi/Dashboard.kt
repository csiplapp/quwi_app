package com.quwi

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils.loadAnimation
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import com.quwi.databinding.ActivityDashboardBinding
import com.quwi.helper.Constant


class Dashboard : AppCompatActivity(), View.OnClickListener {

    var binding: ActivityDashboardBinding? = null
    var isOpen = false

    var fab_open: Animation? = null
    var fab_close: Animation? = null
    var fab_clock: Animation? = null
    var fab_anticlock: Animation? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
        initView()

    }

    private fun initView() {
        fab_close = loadAnimation(applicationContext, R.anim.fab_close)
        fab_open = loadAnimation(applicationContext, R.anim.fab_open)
        fab_clock = loadAnimation(applicationContext, R.anim.fab_rotate_clock)
        fab_anticlock =
                loadAnimation(applicationContext, R.anim.fab_rotate_anticlock)

        val toggle = ActionBarDrawerToggle(
                this, binding!!.mDrawerLayout, null, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        )
        binding!!.mDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        binding!!.linearProjects.setOnClickListener(this)
        binding!!.linearMember.setOnClickListener(this)
        binding!!.fabMain.setOnClickListener(this)
        binding!!.headerLayout.btnNav.setOnClickListener(this)
        binding!!.navView.btnNavLogout.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!) {
            binding!!.linearProjects -> startActivity(
                    Intent(
                            this@Dashboard,
                            ProjectList::class.java
                    )
            )
            binding!!.linearMember -> startActivity(Intent(this@Dashboard, MemberList::class.java))
            binding!!.fabMain -> fabMenuAnimate()
            binding!!.headerLayout.btnNav -> {
                if (!binding!!.mDrawerLayout.isDrawerOpen(Gravity.START)) binding!!.mDrawerLayout.openDrawer(
                        Gravity.START
                )
                else binding!!.mDrawerLayout.closeDrawer(Gravity.END)
            }
            binding!!.navView.btnNavLogout -> {
                closeDrawer()
                logoutDialog().show()
            }
        }
    }

    private fun closeDrawer() {
        if (binding!!.mDrawerLayout.isDrawerOpen(GravityCompat.START)) binding!!.mDrawerLayout.closeDrawer(
                Gravity.LEFT
        )
    }

    private fun logoutDialog(): Dialog {

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.lbl_logout))
        builder.setMessage(getString(R.string.doyouwantlogout))
        builder.setPositiveButton(R.string.lbl_yes) { dialog, which ->
            Constant.clearUserData(this@Dashboard)
            dialog.dismiss()
            val loginIntent = Intent(this@Dashboard, Login::class.java)
            startActivity(loginIntent)
            finish()
        }

        builder.setNegativeButton(R.string.lbl_no) { dialog, which ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        return dialog
    }

    //TODO (Floating Action menu animation show and hide)
    private fun fabMenuAnimate() {
        if (isOpen) {
            binding!!.fabLinearMemb.visibility = View.INVISIBLE
            binding!!.fabLinearProject.visibility = View.INVISIBLE
            binding!!.fabLinearMemb.startAnimation(fab_close)
            binding!!.fabLinearProject.startAnimation(fab_close)
            binding!!.fabMain.startAnimation(fab_anticlock)
            binding!!.fabLinearMemb.isClickable = false
            binding!!.fabLinearProject.isClickable = false
            isOpen = false
        } else {
            binding!!.fabLinearMemb.visibility = View.VISIBLE
            binding!!.fabLinearProject.visibility = View.VISIBLE
            binding!!.fabLinearMemb.startAnimation(fab_open)
            binding!!.fabLinearProject.startAnimation(fab_open)
            binding!!.fabMain.startAnimation(fab_clock)
            binding!!.fabLinearMemb.isClickable = true
            binding!!.fabLinearProject.isClickable = true
            isOpen = true
        }
    }


}