package com.quwi

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.quwi.adapter.CustomAdapter
import com.quwi.connection.ApiCallBack
import com.quwi.connection.ApiConnection
import com.quwi.databinding.ActivityProjectListBinding
import com.quwi.helper.Constant
import com.quwi.helper.Utils
import com.quwi.helper.Utils.convertDateString
import com.quwi.viewholder.ProjectViewHolder
import com.squareup.picasso.Picasso
import okhttp3.Headers
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException


class ProjectList : AppCompatActivity() {

    var binding: ActivityProjectListBinding? = null
    var jsonProjectList = JSONArray()
    var filterProjectList = JSONArray()
    private var customAdapter: CustomAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_project_list)
        initView()
        initToolBar()
    }

    private fun initToolBar() {
        binding!!.headerLayout.txtNavTitle.text = "Projects"
        binding!!.headerLayout.btnNav.setBackgroundResource(R.drawable.ic_baseline_arrow_24)
        binding!!.headerLayout.btnNav.setOnClickListener { finish() }
    }

    private fun initView() {
        binding!!.recyProjectList.setHasFixedSize(true)
        binding!!.recyProjectList.layoutManager = LinearLayoutManager(this)
        binding!!.recyProjectList.itemAnimator = DefaultItemAnimator()
        binding!!.recyProjectList.addItemDecoration(
                DividerItemDecoration(
                        this,
                        DividerItemDecoration.VERTICAL
                )
        )
        binding!!.edProjSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                customAdapter!!.listener.filterData(s.toString())
            }
        })

//        binding!!.edProjSearch.setOnFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
//            if (hasFocus == true) {
//                if (binding!!.edProjSearch.getText().toString().compareTo(getString(R.string.lbl_search)) === 0) {
//                    binding!!.edProjSearch.setText("")
//                    customAdapter!!.listener.filterData("")
//                }else{
//                    customAdapter!!.listener.filterData(binding!!.edProjSearch.text.toString())
//                }
//            }
//        })

        initAdapter()
        getProjectListApiCall()

    }

    private fun getProjectListApiCall() {
        ApiConnection.connectPost(
                this@ProjectList,
                Constant.TAG,
                true,
                null,
                ApiCallBack.getProjects(this@ProjectList),
                object : ApiConnection.ConnectListener {
                    override fun onResponseSuccess(
                            response: String,
                            headers: Headers,
                            StatusCode: Int
                    ) {
                        try {
                            val jobject = JSONObject(response)
                            if (jobject.has("projects") || !jobject.isNull("projects")) {

                                jsonProjectList = jobject.getJSONArray("projects")
                                filterProjectList = jobject.getJSONArray("projects")

                                if(jsonProjectList.length() > 0){
                                    binding!!.txtEmpty.visibility = GONE
                                    binding!!.recyProjectList.visibility = VISIBLE
                                }else{
                                    binding!!.txtEmpty.visibility = VISIBLE
                                    binding!!.recyProjectList.visibility = GONE
                                }
                                customAdapter!!.notifyDataSetChanged()
                            } else {
                                Utils.showSnackBar(binding!!.linearParent, jobject.getString("message"))
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }

                    override fun onResponseFailure(
                            responseData: String,
                            headers: Headers,
                            StatusCode: Int
                    ) {
                        Utils.showSnackBar(binding!!.linearParent, responseData)
                    }

                    override fun onFailure(headers: Headers) {
                        Utils.showSnackBar(binding!!.linearParent, getString(R.string.failure))
                    }

                    override fun onConnectionFailure() {
                        Utils.showSnackBar(binding!!.linearParent, getString(R.string.errorCheckNet))
                    }

                    override fun onException(headers: Headers, StatusCode: Int) {
                        Utils.showSnackBar(binding!!.linearParent, getString(R.string.errorSomething))
                    }
                })
    }

    private fun initAdapter() {
        customAdapter = CustomAdapter(object : CustomAdapter.AdapterListener {
            override fun onCreateViewHolder(
                    parent: ViewGroup,
                    viewType: Int
            ): RecyclerView.ViewHolder {
                return ProjectViewHolder(
                        LayoutInflater.from(parent.context)
                                .inflate(R.layout.project_item, parent, false)
                )
            }

            @SuppressLint("SetTextI18n")
            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                val jObj = filterProjectList.get(position) as JSONObject
                holder as ProjectViewHolder

                holder.txtName.text = jObj.getString("name")
                holder.txtDate.text = convertDateString(jObj.getString("dta_user_since"), true)

                if (!jObj.isNull("logo_url")) {

                    holder.txtProjTitle.visibility = View.GONE
                    holder.imgProfile.visibility = View.VISIBLE
                    Picasso.get().load(jObj.getString("logo_url"))
                            .placeholder(R.drawable.place_holder).error(
                                    R.drawable.place_holder
                            ).into(holder.imgProfile)

                } else {

                    holder.imgProfile.visibility = View.GONE
                    holder.txtProjTitle.visibility = View.VISIBLE
                    holder.txtProjTitle.text = jObj.getString("name").substring(0, 1)

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                        holder.txtProjTitle.background.colorFilter = Utils.colorFilter()
                    }
                }


                if (jObj.getString("is_active") == "1") {

                    holder.linearParent.setBackgroundColor(Color.WHITE)
                    holder.txtStatus.setTextColor(resources.getColor(R.color.dark_green))
                    holder.txtStatus.text = getString(R.string.lbl_active)
                } else {
                    holder.linearParent.setBackgroundColor(resources.getColor(R.color.grey_5))
                    holder.txtStatus.setTextColor(resources.getColor(R.color.grey_40))
                    holder.txtStatus.text = getString(R.string.lbl_close)
                }

                if (jObj.has("users") && jObj.getJSONArray("users").length() > 0) {
                    holder.txtNoWork.text =
                            jObj.getJSONArray("users").length().toString() + " " + getString(
                                    R.string.lbl_workers
                            )
                } else {
                    holder.txtNoWork.text = getString(
                            R.string.lbl_noworkers
                    )
                }

                holder.linearParent.setOnClickListener {
                    val intent = Intent(this@ProjectList, ProjectDetails::class.java)
                    intent.putExtra("projObj", jObj.toString())
                    startActivityForResult(intent, Constant.RESULT_PROJECT_DETAILS)
                }
            }

            override val itemCount: Int
                get() = filterProjectList.length()

            override fun getItemViewType(position: Int): Int {
                return 0
            }

            override fun getItemId(position: Int): Long {
                return 0
            }

            override fun filterData(countryName: String) {
                filterProjectList = JSONArray()
                if (!countryName.equals("")) {
                    for (i in 0 until jsonProjectList.length()) {
                        val jsonObject = jsonProjectList.getJSONObject(i)
                        if (jsonObject.getString("name").toLowerCase().contains(countryName.toLowerCase())) {
                            filterProjectList.put(jsonObject)
                        }
                    }
                }else{
                    filterProjectList = jsonProjectList
                }
                if(filterProjectList.length() > 0){
                    binding!!.txtEmpty.visibility = GONE
                    binding!!.recyProjectList.visibility = VISIBLE
                }else{
                    binding!!.txtEmpty.visibility = VISIBLE
                    binding!!.recyProjectList.visibility = GONE
                }
                customAdapter!!.notifyDataSetChanged()
            }
        })
        binding!!.recyProjectList.adapter = customAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Constant.RESULT_PROJECT_DETAILS) {
            jsonProjectList = JSONArray()
            getProjectListApiCall()
        }
    }

}