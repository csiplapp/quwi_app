package com.quwi

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.quwi.adapter.CustomAdapter
import com.quwi.helper.Utils
import com.quwi.viewholder.MemberViewHolder

class MemberList : AppCompatActivity() {

    var btnNav: ImageView? = null
    var txtNavTitle: TextView? = null
    var recyMemberList: RecyclerView? = null
    var memberList = arrayListOf(
        "Tony Stark",
        "John Doe",
        "Michel Jordan",
        "Baner",
        "Barton Clint",
        "Mack Clark",
        "Natasha",
        "Jack",
        "Andrew",
        "John Smith"
    )
    private var customAdapter: CustomAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_list)
        initView()
//        setOnClickListener()
//
    }

    private fun initView() {
        btnNav = findViewById(R.id.btnNav) as ImageView
        txtNavTitle = findViewById(R.id.txtNavTitle) as TextView
        recyMemberList = findViewById(R.id.recyMemberList)
        recyMemberList!!.setHasFixedSize(true)
        recyMemberList!!.layoutManager = LinearLayoutManager(this)
        recyMemberList!!.itemAnimator = DefaultItemAnimator()
        recyMemberList!!.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        txtNavTitle!!.text = "Members"
        btnNav!!.setBackgroundResource(R.drawable.ic_baseline_arrow_24)
        btnNav!!.setOnClickListener { finish() }
        initAdapter()

    }

    private fun initAdapter() {
        customAdapter = CustomAdapter(object : CustomAdapter.AdapterListener {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): RecyclerView.ViewHolder {
                return MemberViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.member_item, parent, false)
                )
            }

            @RequiresApi(Build.VERSION_CODES.Q)
            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                holder as MemberViewHolder
                holder.txtName.text = memberList.get(position)
                holder.txtProfile.text = memberList.get(position).substring(0,1)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    holder.txtProfile.background.colorFilter = Utils.colorFilter()
                }


            }

            override val itemCount: Int
                get() = 10

            override fun getItemViewType(position: Int): Int {
                return 0
            }

            override fun getItemId(position: Int): Long {
                return 0
            }

            override fun filterData(nameString: String) {

            }
        })
        recyMemberList!!.adapter = customAdapter
    }
}