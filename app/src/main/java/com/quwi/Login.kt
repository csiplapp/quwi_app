package com.quwi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.databinding.DataBindingUtil
import com.quwi.connection.ApiCallBack
import com.quwi.connection.ApiConnection
import com.quwi.databinding.ActivityLoginBinding
import com.quwi.helper.Constant
import com.quwi.helper.Utils
import okhttp3.Headers
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.HashMap

class Login : AppCompatActivity(), View.OnClickListener {

    var binding: ActivityLoginBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        initView()
    }

    private fun initView() {
        binding!!.btnLogin.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v!!) {
            binding!!.btnLogin -> {
                if (isValidate()) {
                    loginApiCall()
                }
            }
        }
    }

    private fun loginApiCall() {

        val map = HashMap<String, String>()
        map["email"] = binding!!.edUserName.text.toString()
        map["password"] = binding!!.edPass.text.toString()

        ApiConnection.connectPost(
            this@Login,
            Constant.TAG,
            true,
            null,
            ApiCallBack.login(map, this@Login),
            object : ApiConnection.ConnectListener {
                override fun onResponseSuccess(
                    response: String,
                    headers: Headers,
                    StatusCode: Int
                ) {
                    val jobject = JSONObject(response)
                    try {
                        if (jobject.has("token") || !jobject.isNull("token")) {

                            Constant.setUserData(this@Login, response)
                            val intent = Intent(this@Login, Dashboard::class.java)
                            startActivity(intent)
                            finish()

                        } else {
                            Utils.showSnackBar(binding!!.relParent, jobject.getString("message"))
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } catch (ej: JSONException) {
                        ej.printStackTrace()
                    }
                }

                override fun onResponseFailure(
                    responseData: String,
                    headers: Headers,
                    StatusCode: Int
                ) {
                    val jobject = JSONObject(responseData).getJSONObject("first_errors")
                    try {
                        if (jobject.has("email") || !jobject.isNull("email")) {
                            Utils.showSnackBar(binding!!.relParent, jobject.getString("email"))
                        } else if (jobject.has("password") || !jobject.isNull("password")) {
                            Utils.showSnackBar(binding!!.relParent, jobject.getString("password"))
                        }
                    } catch (ej: JSONException) {
                        ej.printStackTrace()
                    }
                }

                override fun onFailure(headers: Headers) {
                    Utils.showSnackBar(binding!!.relParent, getString(R.string.failure))
                }

                override fun onConnectionFailure() {
                    Utils.showSnackBar(binding!!.relParent, getString(R.string.errorCheckNet))
                }

                override fun onException(headers: Headers, StatusCode: Int) {
                    Utils.showSnackBar(binding!!.relParent, getString(R.string.errorSomething))
                }
            })
    }

    private fun isValidate(): Boolean {
        if (TextUtils.isEmpty(binding!!.edUserName.text)) {
            Utils.showSnackBar(binding!!.relParent, getString(R.string.err_uname))
            return false
        }
        if (TextUtils.isEmpty(binding!!.edPass.text)) {
            Utils.showSnackBar(binding!!.relParent, getString(R.string.err_pass))
            return false
        }
        return true
    }

}