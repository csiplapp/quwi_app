package com.quwi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.quwi.helper.Constant

class Splash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler(Looper.getMainLooper()).postDelayed({

            if (Constant.checkUserData(this@Splash)) {

                val intent = Intent(this@Splash, Dashboard::class.java)
                startActivity(intent)
                finish()

            } else {
                
                val intent = Intent(this@Splash, Login::class.java)
                startActivity(intent)
                finish()
            }

        }, 3000)

    }
}