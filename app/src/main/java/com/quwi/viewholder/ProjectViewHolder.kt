package com.quwi.viewholder

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.quwi.R
import de.hdodenhof.circleimageview.CircleImageView

class ProjectViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView)  {

    val linearParent = itemView.findViewById<LinearLayout>(R.id.linearParent)
    val txtName = itemView.findViewById<TextView>(R.id.txtName)
    val imgProfile = itemView.findViewById<CircleImageView>(R.id.imgProfile)
    val txtProjTitle = itemView.findViewById<TextView>(R.id.txtProjTitle)
    val txtNoWork = itemView.findViewById<TextView>(R.id.txtNoWork)
    val txtStatus = itemView.findViewById<TextView>(R.id.txtStatus)
    val txtDate = itemView.findViewById<TextView>(R.id.txtDate)
}