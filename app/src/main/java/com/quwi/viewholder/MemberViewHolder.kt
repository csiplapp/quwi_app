package com.quwi.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.quwi.R

class MemberViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView)  {

    val txtName = itemView.findViewById<TextView>(R.id.txtName)
    val txtProfile = itemView.findViewById<TextView>(R.id.txtProfile)

}